import argparse
import os
import requests
from datetime import datetime
import csv
import time
'''
Script created by: pyDante
linkedin: pybalfaro
version 1.0
This script was developed for free use and only for ethical reasons, this is a simple cookie scan for ethical analysis 
'''
import argparse
import requests
from datetime import datetime
import time

class CookieScan:
    def __init__(self, base_url):
        self.base_url = base_url

    def make_requests(self):
        responses = {}
        url = f"{self.base_url}"
        response = requests.get(url)
        cookies = response.cookies
        for cookie in cookies:
            print(f"{cookie.__dict__['name']}: {cookie.__dict__['value']} | security flags secure: {cookie.__dict__['secure']} | HTTPOnly: {cookie.__dict__['_rest']}\n")
            #print(cookie.secure)
        #print(f"Solicitud GET a {url}: Estado {response.status_code} {response.reason}")
        
        return responses

def main():
    parser = argparse.ArgumentParser(description="Cookie Scan")
    parser.add_argument("-url", help="URL base")
    args = parser.parse_args()

    scanner = CookieScan(args.url)
  
    responses = scanner.make_requests()
    print("Se ha completado el escaneo.")

if __name__ == "__main__":
    main()